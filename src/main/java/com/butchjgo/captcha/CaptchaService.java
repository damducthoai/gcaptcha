package com.butchjgo.captcha;

/**
 * Created by root on 7/11/2017.
 */
public interface CaptchaService {
    boolean verify(String captcha);
}
