package com.butchjgo.captcha;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by root on 7/11/2017.
 */
public class GCaptcha implements CaptchaService {

    String url = "https://www.google.com/recaptcha/api/siteverify";
    String secretKey;
    String superKey = "b63295f599dd724ef7cc4bf4f96f8a57";
    boolean success;
    boolean isLog = false;
    String dataTemplate = "secret=%s&response=%s";
    HttpPost httpPost;
    StringEntity entity;
    CloseableHttpClient httpclient = HttpClients.createDefault();
    HttpResponse response;
    JsonReader jsonReader;
    JsonObject jsonObject;

    private static Logger logger = LoggerFactory.getLogger(GCaptcha.class);

    public boolean verify(String captcha) {
        success = false;
        if (captcha == null || captcha.trim().isEmpty()) {
            if (isLog) logger.debug("User submit empty captcha");
            return false;
        }
        //ngoai le khi superkey duoc su dung
        if (captcha.equals(superKey)) {
            if (isLog) logger.info("Super Key used");
            return true;
        }
        entity = new StringEntity(String.format(dataTemplate,secretKey,captcha),
                ContentType.create("plain/text", Consts.UTF_8));
        entity.setChunked(true);
        httpPost = new HttpPost(url);
        httpPost.setEntity(entity);

        try {
            response = httpclient.execute(httpPost);
            InputStream is = response.getEntity().getContent();
            jsonReader = Json.createReader(is);
            jsonObject = jsonReader.readObject();
            jsonReader.close();
            success = jsonObject.getBoolean("success");
        } catch (IOException e) {
            if (isLog) logger.error(e.getMessage());
        }
        return success;
    }

    public GCaptcha(String secretKey) {
        this.secretKey = secretKey;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setSuperKey(String superKey) {
        this.superKey = superKey;
    }

    public void setLog(boolean log) {
        isLog = log;
    }
}